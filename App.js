import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View, FlatList, Image, ActivityIndicator } from 'react-native';

import YoutubeUI from './Tugas/Tugas-12/App';
import LogoinScreen from './Tugas/Tugas-13/LoginScreen';
import AboutScreen from './Tugas/Tugas-13/AboutScreen';
import ToDoApp from './Tugas/Tugas-14/App'
import SkillScreen from './Tugas/Tugas-14/SkillScreen';
import Index from './Tugas/Tugas-15/index';
import MyNavigation from './Tugas/TugasNavigation/index';
import Quiz3 from './Tugas/Quiz-3/index'
import Final from './Tugas/Final_Project/Index'

import Redux from './Tugas/Latihan-Redux/index';

import Axios from 'axios';


export default function App() {
  return (

    // <YoutubeUI />
    // <LogoinScreen />
    // <AboutScreen/>
    // <ToDoApp/>
    // <SkillScreen/>
    // <Index/>
    // <MyNavigation/>
    // <Quiz3/>
    // <Redux/>
    <Final/>
  );
}

// const styles = StyleSheet.create({
//   container: {
//     flex: 1,
//     backgroundColor: '#fff',
//     alignItems: 'center',
//     justifyContent: 'center',
//   },
// });




























// export default class App extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = {
//       data: {},
//       isLoading: true,
//       isError: false
//     };
//   }

//   // Mount User Method
//   componentDidMount() {
//     this.getGithubUser()
//   }

//   //   Get Api Users
//   getGithubUser = async () => {
//     try {
//       const response = await Axios.get(`https://api.github.com/users?since=135`)
//       this.setState({ isError: false, isLoading: false, data: response.data })
//     } catch (error) {
//       this.setState({ isLoading: false, isError: true })
//     }
//   }

//   render() {
//     //  If load data
//     if (this.state.isLoading) {
//       return (
//         <View
//           style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
//         >
//           <ActivityIndicator size='large' color='red' />
//         </View>
//       )
//     }
//     // If data not fetch
//     else if (this.state.isError) {
//       return (
//         <View
//           style={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}
//         >
//           <Text>Terjadi Error Saat Memuat Data</Text>
//         </View>
//       )
//     }
//     // If data finish load
//     return (
//       <FlatList
//         data={this.state.data}
//         renderItem={({ item }) =>
//           <View style={styles.viewList}>
//             <View>
//               <Image source={{ uri: `${item.avatar_url}` }} style={styles.Image} />
//             </View>
//             <View>
//               <Text style={styles.textItemLogin}> {item.login}</Text>
//               <Text style={styles.textItemUrl}> {item.html_url}</Text>

//             </View>
//           </View>
//         }
//         keyExtractor={({ id }, index) => index}
//       />
//     );
//   }
// }

// const styles = StyleSheet.create({
//   viewList: {
//     height: 100,
//     flexDirection: 'row',
//     borderWidth: 1,
//     borderColor: '#DDD',
//     alignItems: 'center'
//   },
//   Image: {
//     width: 88,
//     height: 80,
//     borderRadius: 40
//   },
//   textItemLogin: {
//     fontWeight: 'bold',
//     textTransform: 'capitalize',
//     marginLeft: 20,
//     fontSize: 16
//   },
//   textItemUrl: {
//     fontWeight: 'bold',
//     marginLeft: 20,
//     fontSize: 12,
//     marginTop: 10,
//     color: 'blue'
//   }
// })

























// import { createStore } from 'redux'

// // Membuat action types
// const types = {
//   INCREMENT: 'INCREMENT',
// }

// // Membuat reducer
// const reducer = (state, action) => {
//   if (action.type === types.INCREMENT) {
//     return { count: state.count + 1 }
//   }
//   return state
// }

// // Mendefinisikan initial state dari store
// const initialState = { count: 0 }

// // Membuat store, menambahkan fungsi reducer dan nilai initial state
// const store = createStore(reducer, initialState)

// /// Setup Redux telah selesai, berikut cara menggunakannya:
// // Lakukan dispatch actions, yang terdaftar pada store/reducer
// store.dispatch({ type: types.INCREMENT })
// store.dispatch({ type: types.INCREMENT })
// store.dispatch({ type: types.INCREMENT })

// // Menggunakan store.getState() untuk memperoleh nilai dari object state
//  export default function App() {
//   return (
//     <Text style={{ fontSize: 100 }}>
//       {store.getState().count}
//     </Text>
//   )
// } 

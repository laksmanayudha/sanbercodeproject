import React, {Component} from 'react';
import{
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    StatusBar,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialIcons';



export default class App extends Component{
    render(){
        return(
            
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.props.navigation.toggleDrawer()}>
                    <Icon2 style={{marginTop:-10, marginLeft:10}} name="list" size={30}/>
                </TouchableOpacity>
                <ScrollView>
                    <View style={styles.header}>
                        <Image style={styles.img} source={require('../Tugas-13/images/woman_small.jpg')}/>
                        <Text style={styles.teksProfile}>LAKSMANA YUDHA</Text>
                    </View>
                    <View style={styles.body}>
                        <Text style={styles.judul}>Tentang Saya :</Text>
                        <Text style={styles.paragraf}>
                            Halo ! nama saya  Laksmana Yudha, saya tinggal di Indonesia.Sekarang saya sedang mengerjakan tugas 11 pada website sanbercode yaitu membuat mockup dari suatu aplikasi mobile. Horeee...
                        </Text>

                        <View style={styles.medsosContainer}>
                            <View style={styles.medsosItem}>
                                <Icon name="instagram" size={35}/>
                                <Text style={styles.teksMedsos}>@laksmanayudha_</Text>
                            </View>
                            <View style={styles.medsosItem}>
                                <Icon name="facebook-square" size={35}/>
                                <Text style={styles.teksMedsos}>Laksmana Yudha</Text>
                            </View>
                            <View style={styles.medsosItem}>
                                <Icon name="twitter" size={35}/>
                                <Text style={styles.teksMedsos}>Unknown</Text>
                            </View>
                        </View>

                        <Text style={styles.judul}>Project :</Text>
                        <Text style={styles.paragraf}>
                            Kalian bisa cek project saya di gitlab. berikut linknya :
                            https://gitlab.com/laksmanayudha/imrn0720
                        </Text>
                    </View>
                </ScrollView>
                
                
            </View>
          
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1,
        marginTop: StatusBar.currentHeight
    },
    header:{
        justifyContent:'center',
        alignItems:'center',
        flex:3
    },
    body:{
        flex:5,
        paddingTop:10,
        paddingHorizontal:30
    },
    img:{
        height:150,
        width:150,
        borderRadius:100,
        borderColor: '#7800A1',
        borderWidth:2
    },
    teksProfile:{
        fontSize:20,
        fontWeight: 'bold',
        marginTop:10
    },
    judul:{
        marginTop:10,
        fontSize:18,
        fontWeight: 'bold'
    },
    paragraf:{
        color: '#162183',
        marginTop:5,
        fontSize:15
    },
    medsosContainer:{
        paddingVertical:5
    },
    medsosItem:{
        flexDirection:'row',
        marginTop:15,
        alignItems: 'center'
    },
    teksMedsos:{
        marginLeft:5,
        fontSize:16,
        fontWeight:'normal'
    }

});
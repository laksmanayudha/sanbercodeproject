import React from "react";
import 'react-native-gesture-handler';
import { View, Text, StyleSheet } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Project from './ProjectScreen';
import Add from './AddScreen';
import Skill from './SkillScreen';
import About from './AboutScreen';
import Login from './LoginScreen';

const Tabs = createBottomTabNavigator();
const Drawer = createDrawerNavigator();
const Stack = createStackNavigator();

// const ProjectStackScreen = () =>(
//     <Stack.Navigator>
//             <Stack.Screen name="Project" component={Project} options={{ title: "Project Saya" }} />
//     </Stack.Navigator>
// )

const TabsScreen = () =>(
    <Tabs.Navigator initialRouteName={Skill} >
        <Tabs.Screen name="Skill" component={Skill}/>
        <Tabs.Screen name="Project" component={Project}/>
        <Tabs.Screen name="Add" component={Add}/>
    </Tabs.Navigator>
)

const DrawerScreen = () => (
    <Drawer.Navigator>
        <Drawer.Screen name="Skill" component={TabsScreen}/>
        <Drawer.Screen name="About" component={About}/>
    </Drawer.Navigator>
)


export default () => (

    <NavigationContainer>
        <Stack.Navigator>
            <Stack.Screen name="login" component={Login} options={{ title: "Login" }} />
            <Stack.Screen name="Skill" component={DrawerScreen} options={{ title: "Sanber App" }}/>
        </Stack.Navigator>
    </NavigationContainer>

)

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems: 'center',
        justifyContent: 'center'
    }
   });
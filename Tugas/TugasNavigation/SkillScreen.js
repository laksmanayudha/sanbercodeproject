import React from 'react';
import { View, Text, ScrollView, TextInput, TouchableOpacity, StyleSheet, FlatList, StatusBar, Image } from 'react-native';
import Icon from 'react-native-vector-icons/MaterialIcons';
import Icon2 from 'react-native-vector-icons/MaterialCommunityIcons';
import data from './skillData.json';

export class SkillItem extends React.Component {
    
    render(){
        let skill = this.props.skill;
        return (
            <TouchableOpacity style={styles.skillItem}>
                <View style={styles.skillDescription}>
                    <Text>{skill.id}</Text>
                </View>
                <View style={styles.skillDescription}>
                    <Icon2 name={skill.iconName} size={80}/>
                </View>
                <View style={styles.skillDescription}>
                    <Text style={{fontSize:18, fontWeight:'bold'}}>{skill.skillName}</Text>
                    <Text>{`Kategori : ${skill.categoryName}`}</Text>
                    <Text>{`Project : ....`}</Text>
                    <Text>{`Tingkat Penguasaan : ${skill.percentageProgress}`}</Text>
                </View>
            </TouchableOpacity>
        )
    }

}

export default class SkillScreen extends React.Component {

    render(){
      return(
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity style={styles.tombolKembali} onPress={() => this.props.navigation.toggleDrawer()}>
                    <Icon name="list" size={30}/>
                </TouchableOpacity>
                <View style={styles.teksProfilSayaContainer}>
                    <Text style={styles.teksProfilSaya}>PROFIL SAYA</Text>
                </View>
            </View>
            <View style={styles.profileContainer}>
                <Image style={styles.fotoProfile} source={require('../Tugas-13/images/woman_small.jpg')}/>
                <View style={styles.deskripsiProfile}>
                    <View>
                        <Text style={styles.username}>Katherine Heigl</Text>
                        <Text style={styles.email}>katherineheigl@gmailcom</Text>
                    </View>
                    <View style={styles.totalSkill}>
                        <Text style={{color:'white', fontSize:12}}>6 Skills</Text>
                    </View>
                </View>
            </View>
            <Text style={{fontSize:16, marginLeft:20, fontWeight:'200'}}>Daftar Skill : </Text>
            <View style={styles.body}>
                <View style={styles.kategori}>
                    <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                        <TouchableOpacity >
                            <Text style={styles.elemenKategori}>Semua</Text>
                        </TouchableOpacity >
                        <TouchableOpacity >
                            <Text style={styles.elemenKategori}>Bahasa Pemrograman</Text>
                        </TouchableOpacity>
                        <TouchableOpacity >
                            <Text style={styles.elemenKategori}>Framework/library</Text>
                        </TouchableOpacity>
                        <TouchableOpacity >
                            <Text style={styles.elemenKategori}>Teknologi</Text>
                        </TouchableOpacity>
                    </ScrollView>
                    
                </View>
                <View style={styles.listSkill}>
                    {/* <SkillItem skill={data.items[0]}/> */}
                    <FlatList
                    data = {data.items}
                    renderItem = {(skill) => <SkillItem skill={skill.item}/>}
                    keyExtractor={(item)=>item.id}
                    ItemSeparatorComponent={() => <View style={{height:0.5, backgroundColor:"#E5E5E5"}} />}
                    />
                </View>
            </View>
            {/* <View style={styles.tabBAr}>
                <TouchableOpacity>
                    <Icon name="home" size={30}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name="search" size={30}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name="settings" size={30}/>
                </TouchableOpacity>
            </View> */}
        </View>
      );

    }
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: StatusBar.currentHeight,
    backgroundColor: '#FAFAFA'
    // backgroundColor:'red'
  },
  header:{
      flexDirection: 'row',
      backgroundColor: 'white',
      height:50,
      alignItems:'center',
      justifyContent: 'center'
  },
  teksProfilSaya:{
      fontSize: 14,
      fontWeight: 'bold',
  },
  tombolKembali:{
      position: 'absolute',
      left:10
  },
  fotoProfile:{
      height:110,
      width:110,
      flex:1
  },
  profileContainer:{
      height:150,
      backgroundColor:'white',
      marginHorizontal:20,
      marginVertical:10,
      padding:20,
      borderRadius:10,
      flexDirection:'row'
  },
  deskripsiProfile:{
      marginLeft:10,
      justifyContent:'space-between',
      flex:2
  },
  username:{
    fontSize:18,
    fontWeight: 'bold'
  },
  email:{
    color:'#4D4D4D'
  },
  totalSkill:{
      justifyContent:'center',
      alignItems:'center',
      backgroundColor:'#2300FC',
      height:30,
      borderRadius:5
  },
  body:{
      flex:1,
      backgroundColor:'white',
      borderRadius:10,
      marginTop:10,
      paddingHorizontal:20,
      paddingVertical:10
  },
  kategori:{
    flexDirection:'row',
    borderBottomWidth:0.5,
    borderBottomColor:'#9DBEF0',
    padding:10
  },
  elemenKategori:{
    marginRight:15,
    fontSize:16
  },
  tabBAr:{
      position:'absolute',
      bottom:0,
      left:0,
      right:0,
      backgroundColor:'white',
      height:65,
      borderTopWidth:0.5,
      borderTopColor:'#9DBEF0',
      flexDirection:'row',
      justifyContent:'space-around',
      alignItems: 'center'
  },
  skillItem:{
      flexDirection:'row',
      padding:10,
      marginTop:5
  },
  skillDescription:{
      marginRight:10
  }



});


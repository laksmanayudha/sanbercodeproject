import React, { version } from 'react';
import { TextInput } from 'react-native-gesture-handler';
import { StyleSheet, Image, Text, View, TouchableOpacity } from 'react-native';

export default ({navigation}) => (

    <View style={styles.container}>
        <View style={styles.header}>
            <View style={styles.teksContainer}>
                <Text style={styles.welcomeBack}>Welcome Back</Text>
                <Text style={{color: '#4D4D4D', fontSize:12}}>Sign in to continue</Text>
            </View>
            
        </View>
        <View style={styles.body}>
            <View style={styles.boxLuar}>
                <View style={styles.userInput}>
                    <Text style={{fontSize:12}}>Email</Text>
                    <TextInput style={styles.teksInput}></TextInput>
                    <View style={{height:0.5, backgroundColor:'#E6EAEE'}}/>
                </View>
                <View style={styles.userInput}>
                    <Text style={{fontSize:12}}>Password</Text>
                    <TextInput style={styles.teksInput}></TextInput>
                    <View style={{height:0.5, backgroundColor:'#E6EAEE'}}/>
                </View>
                <View style={{alignItems: 'flex-end'}}>
                    <Text style={{fontSize:12}}>Forgot Password?</Text>
                </View>
                <TouchableOpacity onPress={() => navigation.push('home')}>
                    <View style={styles.SignIn}>
                        <Text style={{color: '#FFFFFF'}}>Sign In</Text>
                    </View>
                </TouchableOpacity>
                <View style={{alignItems:'center'}}>
                    <Text style={{fontSize:15}}>- OR -</Text>
                </View>
                
                <View style={styles.akunLain}>
                    <TouchableOpacity>
                        <View style={styles.logo}>
                            <Image source={require('./images/001-facebook-1.png')}/>
                            <Text style={{marginLeft:10}}>Facebook</Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <View style={styles.logo}>
                            <Image source={require('./images/003-search.png')}/>
                            <Text style={{marginLeft:10}}>Google</Text>
                        </View>
                    </TouchableOpacity>

                </View>
            </View>
        </View>
        <View style={styles.footer}>
            <View style={styles.garisBawah}/>
        </View>
       
    </View>

)

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        backgroundColor: '#FEFEFE'
    },
    header:{
        flex:2
    },
    body:{
        flex:12
    },
    teksContainer:{
        position: 'absolute',
        left:0,
        bottom:0
    },
    welcomeBack:{
        fontWeight: 'bold',
        fontSize: 30,
        color:'#0C0423'
    },
    boxLuar:{
        backgroundColor: '#FFFFFF',
        height:100,
        elevation: 5,
        borderRadius:18,
        marginTop:24,
        padding:20,
        height: 498,
        justifyContent:'space-around',
        
    },
    SignIn:{
        justifyContent:'center',
        alignItems: 'center',
        height: 50,
        width: 318,
        backgroundColor:'#F77866',
        borderRadius:6
    },
    logo:{
        flexDirection: 'row',
        width:149,
        height:44,
        borderWidth:1,
        justifyContent: 'center',
        alignItems:'center',
        borderColor:'#E6EAEE',
        borderRadius:6
    },
    akunLain:{
        flexDirection:'row',
        justifyContent: 'space-between',
        padding:10
    },
    garisBawah:{
        backgroundColor:'#000000',
        height:5,
        width:134,
        borderRadius:2.5,
    },
    footer:{
        alignItems:'center'
    }

})
import React from "react";
import 'react-native-gesture-handler';
import { View, Text, StyleSheet } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Login from './Login';
import Register from './Register';
import HomeScreen from './HomeScreen';

const Stack = createStackNavigator();

export default () => (

    <NavigationContainer>
        <Stack.Navigator initialRouteName={Register}>
            <Stack.Screen name="register" component={Register} options={{headerShown: false}} />
            <Stack.Screen name="login" component={Login} options={{title: "Login"}} />
            <Stack.Screen name="home" component={HomeScreen} options={{headerShown: false}} />
        </Stack.Navigator>
    </NavigationContainer>

)
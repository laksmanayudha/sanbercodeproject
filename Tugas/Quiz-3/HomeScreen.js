import React from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, ScrollView } from 'react-native';
import { TextInput } from 'react-native-gesture-handler';
// import symbolicateStackTrace from 'react-native/Libraries/Core/Devtools/symbolicateStackTrace';
import Icon from 'react-native-vector-icons/AntDesign'

export default () => (

    <View style={styles.container}>
        <View style={styles.searchBar}>
            <View style={styles.search}>
                <Icon name="search1" size={25}/>
                <TextInput placeholder="Search Product"></TextInput>
                <Icon name="camera" size={25}/>
            </View>
            <View style={{justifyContent:'center', alignItems:'center'}}>
                <Icon name="bells" size={20}/>
            </View>      
        </View>
        <View style={styles.content}>
            <View style={[styles.slider, {marginTop:15}]}>
                <Image source={require('./images/Slider.png')} style={{width:382, height:190}}/>
            </View>
            <View style={[styles.categories, {marginTop:15}]}>
                <View style={styles.categoryItem}>
                    <Image source={require('./images/women.png')}/>
                    <Text style={{color:'#616D80', fontSize:18}}>Woman</Text>
                </View>
                <View style={styles.categoryItem}>
                    <Image source={require('./images/Man.png')}/>
                    <Text style={{color:'#616D80', fontSize:18}}>Man</Text>
                </View>
                <View style={styles.categoryItem}>
                    <Image source={require('./images/shoe.png')}/>
                    <Text style={{color:'#616D80', fontSize:18}}>Shoe</Text>
                </View>
                <View style={styles.categoryItem}>
                    <Image source={require('./images/kids.png')}/>
                    <Text style={{color:'#616D80', fontSize:18}}>Kids</Text>
                </View>
                <View style={styles.categoryItem}>
                    <Image source={require('./images/more.png')}/>
                    <Text style={{color:'#616D80', fontSize:18}}>More</Text>
                </View>
            </View>
            <View style={[styles.flashSell, {marginTop:15}]}>
                 <View style={styles.judul}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:24, color:'#4D4D4D'}}> Flash Sell </Text>
                        <Text  style={{fontSize:12, color:'#F89C52'}}> 03.30.30 </Text>
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:15, color:'#4D4D4D'}}> All </Text>
                        <Icon name="right" size={15} style={{color:'#F89C52'}}/>
                    </View>
                </View>
                
                <View style={styles.isi}>
                     <ScrollView horizontal={true}>
                        <View style={styles.item}>
                            <Image source={require('./images/handwash.png')} style={{width:120, height:117}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                        <View style={styles.item}>
                            <Image source={require('./images/handwash.png')} style={{width:120, height:117}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                        <View style={styles.item}>
                            <Image source={require('./images/handwash.png')} style={{width:120, height:117}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                        <View style={styles.item}>
                            <Image source={require('./images/handwash.png')} style={{width:120, height:117}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                    </ScrollView>
                </View>
                
            </View>
            <View style={[styles.newProduct, {marginTop:15}]}>
                 <View style={styles.judul}>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:24, color:'#4D4D4D'}}> New Product </Text>
                        
                    </View>
                    <View style={{flexDirection:'row', alignItems:'center'}}>
                        <Text style={{fontSize:15, color:'#4D4D4D'}}> All </Text>
                        <Icon name="right" size={15} style={{color:'#F89C52'}}/>
                    </View>
                </View>
                
                <View style={styles.isi}>
                     <ScrollView horizontal={true}>
                        <View style={styles.itemNew}>
                            <Image source={require('./images/handwash.png')} style={{width:183, height:150}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                        <View style={styles.itemNew}>
                            <Image source={require('./images/handwash.png')} style={{width:183, height:150}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                        <View style={styles.itemNew}>
                            <Image source={require('./images/handwash.png')} style={{width:183, height:150}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                        <View style={styles.itemNew}>
                            <Image source={require('./images/handwash.png')} style={{width:183, height:150}}/>
                            <View style={styles.description}>
                                <Text style={{fontSize:12, color:'#575757'}}>Tiare Handwash</Text>
                                <Text style={{fontSize:12, color:'#323232'}}>$12.22</Text>
                            </View>
                            
                        </View>
                    </ScrollView>
                </View>
                
            </View>
            
        </View>
        <View style={styles.tabBAr}>
            <TouchableOpacity>
                <Icon name="home" size={30}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Icon name="shoppingcart" size={30}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Icon name="message1" size={30}/>
            </TouchableOpacity>
            <TouchableOpacity>
                <Icon name="profile" size={30}/>
            </TouchableOpacity>
        </View>
    </View>

)

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor: '#FEFEFE'
    },
    search:{
        flexDirection:'row',
        justifyContent:'space-between',
        width:352,
        height:44,
        paddingHorizontal:10,
        alignItems:'center',
        borderWidth:1,
        borderRadius:10,
        borderColor:'#727C8E'
    },
    searchBar:{
        flexDirection:'row',
        justifyContent:'space-between',
    },
    categoryItem:{
        justifyContent:'center',
        alignItems:'center'
    },
    categories:{
        flexDirection:'row'
    },
    judul:{
        flexDirection:'row',
        justifyContent:'space-between'
    },
    description:{
        padding:10
    },
    item:{
        width:120,
        height:170,
        elevation:1,
        marginRight:5
    },
    itemNew:{
        width:183,
        height:150,
        elevation:1,
        marginRight:5
    },
     tabBAr:{
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
        backgroundColor:'white',
        height:65,
        borderTopWidth:0.5,
        borderTopColor:'#9DBEF0',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems: 'center'
    },
})
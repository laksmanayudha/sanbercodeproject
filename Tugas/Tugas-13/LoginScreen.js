import React, {Component} from 'react';
import{
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import { StatusBar } from 'expo-status-bar';



export default class App extends Component{
    render(){
        return(
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.logo}></View>
                    <Text style={styles.teksLogo}>SANBER APP</Text>
                </View>
                <View style={styles.body}>
                    <View style={styles.inputContainer}>
                        <Text>Username/Email :</Text>
                        <TextInput style={styles.userInput}/>
                    </View>
                    <View style={styles.inputContainer}>
                        <Text>Password : </Text>
                        <View style={[styles.userInput, {flexDirection:'row', justifyContent:'space-between'}]}>
                            <TextInput style={{flex:9}}/>
                            <TouchableOpacity style={{flex:1, justifyContent:'center', alignItems:'center'}}>
                                <Icon name="eye" size={20}/>
                            </TouchableOpacity> 
                        </View>
                    </View>
                    <TouchableOpacity>
                        <View style={styles.loginButton}>
                            <Text style={styles.teksLogin}>LOGIN</Text>
                        </View>
                    </TouchableOpacity>
                    <View style={styles.inputContainer}>
                        <TouchableOpacity>
                            <Text>Belum punya akun? Silahkan register.</Text>
                            <View style={{backgroundColor: 'black', height:0.5}}/>
                        </TouchableOpacity>
                    </View>
                    <View style={styles.inputContainer}>
                        <TouchableOpacity>
                            <Text style={styles.teksTentangSaya}>Tentang Saya</Text>
                        </TouchableOpacity>
                    </View>
                </View>
               
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    header:{
        // backgroundColor:'red',
        flex:3,
        justifyContent:'center',
        alignItems: 'center',
    },
    body:{
        flex:5,
        flexDirection:"column",
        alignItems: 'center',
        justifyContent:'space-around'
    },
    logo:{
        width:120,
        height:100,
        backgroundColor: '#1713D6',
        borderRadius: 12,
        marginTop: 50,
        borderWidth: 2,
        borderColor: 'green'
    },
    teksLogo:{
        color: '#0015D7',
        fontWeight: 'bold',
        fontSize: 24,
        paddingTop:5
    },
    userInput:{
        height: 50,
        width:320,
        backgroundColor: '#DBEAF8',
        marginTop:5,
        padding:10
    },
    loginButton:{
        height:40,
        width:180,
        backgroundColor: '#283EFF',
        justifyContent: 'center',
        alignItems:'center',
        borderRadius:10
    },
    teksLogin:{
        color:'white',
        fontSize: 14
    },
    teksTentangSaya:{
        color: '#0037C5',
        fontSize:18
    }
});
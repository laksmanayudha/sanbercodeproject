import React from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, ScrollView, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';


export default({route}) => {

    let movieItem = route.params.movieItem;

    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={styles.teksMakanan}>{movieItem.Title}</Text>
            </View>
            <View style={styles.body}>
                <View style={styles.imageContainer} >
                    <Image style={styles.image} source={{uri:movieItem.Poster}}/>
                </View>     
                <View style={styles.detailContainer}>
                    <View style={styles.descriptionContainer}>
                        <Text style={styles.paragraf}>{`Tittle : ${movieItem.Title}`}</Text>
                        <Text style={styles.paragraf}>{`Year : ${movieItem.Year}`}</Text>
                        <Text style={styles.paragraf}>{`Type : ${movieItem.Type}`}</Text>
                        <Text style={styles.paragraf}>Description :</Text>
                        <Text style={styles.paragraf}>Movies Description Here...</Text>
                    </View>
                    
                </View>
            </View>
            <View style={styles.footer}>
                <TouchableOpacity>
                    <View style={styles.addCart}>
                        <Text style={styles.teksAddCart}>+   Nonton sekarang</Text>
                    </View>
                </TouchableOpacity>
                <View style={styles.hargaContainer}>
                    <Text style={styles.teksHarga}>Rp 12.0000, 00</Text>
                </View>
            </View>
        </View>
    )

}


const styles = StyleSheet.create({
    container:{
        flex:1
    },
    header:{
        flex:10,
        justifyContent: 'flex-end',
        padding:20
    },
    body:{
        flex:75,
    },
    footer:{
        flex:15,
        flexDirection:'row',
        justifyContent:'space-evenly',
        alignItems:'center'
    },
    teksMakanan:{
        fontSize:24,
        fontWeight:'bold'
    },
    imageContainer:{
        alignItems:'center',
        borderWidth:0.5,
        borderColor:'#C4C4C4'
    },
    image:{
        width:300,
        height:300
    },
    detailContainer:{
        padding:10,
        flexDirection:'row',
        justifyContent:'space-between'
    },
    tambah:{
        flex:2,
        alignItems:'center',
        justifyContent:'space-around',
        height:150

    },
    tombol:{
        borderRadius:50,
        elevation:5,
        backgroundColor: '#FFFFFF',
        padding:6,
        height:40,
        width:40,
        justifyContent:'center',
        alignItems:'center'
    },
    descriptionContainer:{
        flex:8,
        padding:5
    },
    paragraf:{
        color:'#666666',
        fontSize:14,
        marginTop:5
    },
    jumlah:{
        fontSize:16
    },
    addCart:{
        backgroundColor:'#4B7EFF',
        height:45,
        width:200,
        justifyContent:'center',
        alignItems:'center',
        borderRadius:12
    },
    teksAddCart:{
        color:'#FFFFFF',
        fontSize:16,
        fontWeight:'bold'
    },
    teksHarga:{
        color:'#E76F00',
        fontSize:18,
        fontWeight:'bold'
    }
})
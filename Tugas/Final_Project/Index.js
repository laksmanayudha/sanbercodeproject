import React, { useEffect } from "react";
import 'react-native-gesture-handler';
import { View, Text, StyleSheet } from "react-native";
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { createDrawerNavigator } from '@react-navigation/drawer';

import Login from './Login';
import About from './About';
import Home from './Home';
import Detail from './Detail';
import Splash from './Splash';

const Stack = createStackNavigator()



export default () => {

    return(
        // <Login/>
        // <About/>
        // <Home/>
        // <Detail/>
        // <Splash/>
    
        <NavigationContainer>
            <Stack.Navigator initialRouteName="Splash">
                <Stack.Screen name="Splash" component={Splash} options={{headerShown: false}} />
                <Stack.Screen name="Login" component={Login} options={{headerShown: false}} />
                <Stack.Screen name="About" component={About} options={{title: "Login"}} />
                <Stack.Screen name="Home" component={Home} options={{headerShown: false}} />
                <Stack.Screen name="Detail" component={Detail} options={{headerShown: false}} />
            </Stack.Navigator>
        </NavigationContainer>
    )

}


const styles = StyleSheet.create({
    container:{
        flex:1,
        justifyContent: 'center',
        alignItems: 'center'
    }
})
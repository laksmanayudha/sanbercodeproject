import React from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, ScrollView, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialIcons';
import MovieItem from './movieItems';

import Coba from './Coba.json'

export default class Home extends React.Component{

    state = {
        data:[]
    }

    componentDidMount(){
        this.fetchData();
    }

    fetchData = async () => {
        const response = await fetch('http://www.omdbapi.com/?apikey=29da3032&s=love&plot=full');
        const json = await response.json();
        this.setState({ data:json.Search });
    }

    render(){
        return (

        <View style={styles.container}>
            <View style={styles.greetContainer}>
                <Text style={styles.userGreet}>Hi <Text style={{fontWeight:'bold'}}>Yudha</Text> !</Text>
            </View>
            <View style={styles.searchBar}>
                <Icon name="search1" size={25}/>
                <TextInput placeholder="Search Product" style={{marginLeft:10, flex:1}}></TextInput>   
            </View>
            <View style={styles.content}>
                <ScrollView showsHorizontalScrollIndicator={false}>

                    <View style={[styles.slider, {marginTop:15}]}>
                        <ScrollView horizontal={true}>
                            <Image source={require('./images/slider2.png')} style={styles.slider}/>
                            <Image source={require('./images/slider2.png')} style={styles.slider}/>
                        </ScrollView>
                        
                    </View>
                    <View style={[styles.categories, {marginTop:15}]}>
                        <View style={styles.judul}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:14, color:'#4D4D4D'}}> Genre </Text>
                            </View>
                        </View>
                        <View style={styles.isi}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity>
                                    <View style={styles.categoryItem}>
                                        <Text style={styles.teksCategories}>Action</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={styles.categoryItem}>
                                        <Text style={styles.teksCategories}>Drama</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={styles.categoryItem}>
                                        <Text style={styles.teksCategories}>Comedy</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={styles.categoryItem}>
                                        <Text style={styles.teksCategories}>Thriller</Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity>
                                    <View style={styles.categoryItem}>
                                        <Text style={styles.teksCategories}>Horor</Text>
                                    </View>
                                </TouchableOpacity>           
                            </ScrollView>
                        </View>
                        
                    </View>
                    <View style={[styles.topMovies, {marginTop:15}]}>
                        <View style={styles.judul}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:24, color:'#4D4D4D'}}> Top Movies </Text>
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:15, color:'#4D4D4D'}}> All </Text>
                                <Icon name="right" size={15} style={{color:'#F89C52'}}/>
                            </View>
                        </View>
                        
                        <View style={styles.isi}>
                            <FlatList
                            horizontal={true}
                            data = {this.state.data}
                            renderItem = {(movie) => <MovieItem movie={movie.item} pushMethod={() => this.props.navigation.push('Detail', { movieItem: movie.item })}/>}
                            keyExtractor={(item)=>item.imdbID}
                            />
                        </View>
                        
                    </View>
                    <View style={[styles.newProduct, {marginTop:15}]}>
                        <View style={styles.judul}>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:24, color:'#4D4D4D'}}> New Product </Text>
                                
                            </View>
                            <View style={{flexDirection:'row', alignItems:'center'}}>
                                <Text style={{fontSize:15, color:'#4D4D4D'}}> All </Text>
                                <Icon name="right" size={15} style={{color:'#F89C52'}}/>
                            </View>
                        </View>
                        
                        <View style={styles.isi}>
                            <ScrollView horizontal={true} showsHorizontalScrollIndicator={false}>
                                <TouchableOpacity>
                                    <View style={styles.itemNew}>
                                        <Image source={require('./images/avengers1.jpg')} style={{width:183, height:150}}/>
                                        <View style={styles.description}>
                                            <Text style={styles.teksJudulItem}>The Avengers</Text>
                                            <Text style={styles.teksHargaItem}>Rp 12.000, 00</Text>
                                        </View>
                                        
                                    </View>
                                </TouchableOpacity>
                                
                                
                            
                            </ScrollView>
                        </View>
                        
                    </View>

                </ScrollView>
                
                
            </View>
            <View style={styles.tabBAr}>
                <TouchableOpacity>
                    <Icon name="home" size={30}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name="shoppingcart" size={30}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon name="hearto" size={30}/>
                </TouchableOpacity>
                <TouchableOpacity>
                    <Icon2 name="history" size={30}/>
                </TouchableOpacity>
            </View>
        </View>

        )
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor: '#FEFEFE'
    },
    searchBar:{
        flexDirection:'row',
        padding:15,
        backgroundColor:'#F5F5F5',
        borderRadius:10,
        height:60,
        alignItems:'center',
    },
    categoryItem:{
        justifyContent:'center',
        alignItems:'center',
        width:90,
        height:40,
        backgroundColor:'#F5F5F5',
        marginRight:10,
        borderRadius:6,
        padding:10
    },
    isi:{
        flexDirection:'row',
        paddingTop:10
    },
    judul:{
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor:'#C4C4C4',
        borderBottomWidth:1,
        padding:10
    },
    description:{
        padding:10
    },
    item:{
        width:120,
        height:170,
        elevation:1,
        marginRight:5
    },
    itemNew:{
        width:183,
        height:150,
        elevation:1,
        marginRight:5
    },
     tabBAr:{
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
        backgroundColor:'white',
        height:65,
        borderTopWidth:0.5,
        borderTopColor:'#C0C0C0',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems: 'center'
    },
    slider:{
        marginRight:10
    },
    teksCategories:{
        color:'#616D80', fontSize:16
    },
    userGreet:{
        fontSize:30
    },
    greetContainer:{
        padding:10,
        height:80,
        justifyContent:'flex-end'
    },
    teksJudulItem:{
        fontSize:12, 
        color:'#575757'
    },
    teksHargaItem:{
        fontSize:12, color:'#323232'
    },
    tombolPlus:{
        backgroundColor:'#4B7EFF',
        padding:5,
        borderRadius:50
    },
    descriptionContainer:{
        flexDirection:'row',
        alignItems:'center'
    }
})
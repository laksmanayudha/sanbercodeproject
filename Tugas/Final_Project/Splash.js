import React, {useEffect} from 'react';
import {View, Text, StyleSheet, Image} from 'react-native';




export default ({ navigation }) => {

    useEffect(() => {
        setTimeout(() => {
            navigation.navigate('Login');
        }, 3000);
    
    })


    return(
        <View style={styles.continer}>
            <View style={styles.body}>
                <View style={styles.backGroundElipse}>
                    <Image source={require('./images/logo.png')}/>
                </View>
            </View>
            <View style={styles.footer}> 
                <View style={styles.garisBawah}/>        
            </View>
        </View>
    )

} 


const styles = StyleSheet.create({
    continer:{
        flex:1
    },
    garisBawah:{
        backgroundColor:'#228DF0',
        height:5,
        width:134,
        borderRadius:2.5,
    },
    footer:{
        alignItems:'center'
    },
    body:{
        flex:1,
        justifyContent:'center',
        alignItems:'center'
    },
    backGroundElipse:{
        backgroundColor:'#9896FF',
        height:200,
        width:200,
        borderRadius:200,
        justifyContent:'center',
        alignItems:'center'
    }

})
import React, { version } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Image} from 'react-native';

export default ({navigation}) => (

    <View style={styles.container}>
        <View style={styles.header}>
            <View style={styles.logo} >
                <Image  source={require('./images/logo.png')}/>
            </View> 
            
        </View>
        <View style={styles.teksContainer}>
                <Text style={styles.welcomeBack}>Welcome to MyApp</Text>
                <Text style={{color: '#4D4D4D', fontSize:12}}>Login to continue</Text>
        </View>
        <View style={styles.body}>
            <View style={styles.boxLuar}>
                <View style={styles.userInput}>
                    <Text style={styles.teks}>Username/email</Text>
                    <View style={styles.teksInput}>
                        <TextInput ></TextInput>
                    </View>
                </View>
                <View style={styles.userInput}>
                    <Text style={styles.teks}>Password</Text>
                    <View style={styles.teksInput}>
                        <TextInput ></TextInput>
                    </View>
                </View>
                <TouchableOpacity>
                    <View style={{alignItems: 'flex-end'}}>
                        <Text style={styles.teks}>Forgot Password?</Text>
                    </View>
                </TouchableOpacity>  
                <TouchableOpacity style={styles.tombolLogin} onPress={()=>navigation.push('Home')}>
                    <View style={styles.login}>
                        <Text style={{color: '#FFFFFF'}}>Login</Text>
                    </View>
                </TouchableOpacity>

                <TouchableOpacity style={{alignItems: 'center'}}>
                    <View>
                        <Text style={styles.teks}>Belum punya akun ? silahkan <Text style={{color:'#8C8AFF'}}>register</Text></Text>
                        <View style={{backgroundColor:'#A7A7A7',height:0.5, width:200}}/>
                    </View>                  
                </TouchableOpacity>
                
            </View>
        </View>
        <View style={styles.footer}>
            <TouchableOpacity onPress={()=>navigation.push('About')}>
                <Text style={[styles.teks, {fontSize:16}]}>About Me</Text>
            </TouchableOpacity>        
            <View style={styles.garisBawah}/>
            
        </View>
       
    </View>

)

const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:20,
        backgroundColor: '#FEFEFE'
    },
    header:{
        flex:2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    body:{
        flex:7
    },
    teksContainer:{
        
    },
    welcomeBack:{
        fontWeight: 'bold',
        fontSize: 24,
        color:'#0C0423'
    },
    boxLuar:{
        backgroundColor: '#FFFFFF',
        elevation: 5,
        borderRadius:18,
        marginTop:24,
        padding:20,
        height: 450,
        justifyContent:'space-around',
        
    },
    login:{
        justifyContent:'center',
        alignItems: 'center',
        height: 50,
        width: 200,
        backgroundColor:'#228DF0',
        borderRadius:6
    },
    logo:{
        flexDirection: 'row',
        width:100,
        height:100,
        justifyContent: 'center',
        alignItems:'center',
    },
    garisBawah:{
        backgroundColor:'#228DF0',
        height:5,
        width:134,
        borderRadius:2.5,
    },
    footer:{
        alignItems:'center'
    },
    teksInput:{
        height:45,
        borderWidth:0.3,
        padding:10
    },
    tombolLogin:{
        alignItems:'center'
    },
    teks:{
        fontSize:12,
        marginBottom:5
    }

})
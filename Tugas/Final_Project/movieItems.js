import React from 'react';
import { StyleSheet, Image, Text, View, TouchableOpacity, ScrollView, FlatList, TextInput } from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialIcons';

export default class MovieItems extends React.Component {
    
    render(){
        let movie = this.props.movie;
        let pushMethod = this.props.pushMethod;
        return (
            <TouchableOpacity onPress={pushMethod}>
                <View style={styles.item}>
                    <Image source={{uri:movie.Poster}} style={{width:120, height:117}}/>
                    <View style={styles.descriptionContainer}>
                        <View style={styles.description}>
                            <Text style={styles.teksJudulItem}>{movie.Title}</Text>
                            <Text style={styles.teksHargaItem}>Rp 12.000, 00</Text>
                        </View>
                        {/* <TouchableOpacity style={styles.tombolPlus}>
                            <View style={styles.plus}>
                                <Icon name="plus" size={15} style={{color:'#FFFFFF'}}/>
                            </View>
                        </TouchableOpacity> */}
                    </View>
                </View>
            </TouchableOpacity>              
        )
    }

}


const styles = StyleSheet.create({
    container:{
        flex:1,
        padding:15,
        backgroundColor: '#FEFEFE'
    },
    searchBar:{
        flexDirection:'row',
        padding:15,
        backgroundColor:'#F5F5F5',
        borderRadius:10,
        height:60,
        alignItems:'center',
    },
    categoryItem:{
        justifyContent:'center',
        alignItems:'center',
        width:90,
        height:40,
        backgroundColor:'#F5F5F5',
        marginRight:10,
        borderRadius:6,
        padding:10
    },
    isi:{
        flexDirection:'row',
        paddingTop:10
    },
    judul:{
        flexDirection:'row',
        justifyContent:'space-between',
        borderBottomColor:'#C4C4C4',
        borderBottomWidth:1,
        padding:10
    },
    description:{
        padding:10,
        flex:8
    },
    item:{
        width:120,
        height:200,
        elevation:1,
        marginRight:5
    },
    itemNew:{
        width:183,
        height:150,
        elevation:1,
        marginRight:5
    },
     tabBAr:{
        position:'absolute',
        bottom:0,
        left:0,
        right:0,
        backgroundColor:'white',
        height:65,
        borderTopWidth:0.5,
        borderTopColor:'#C0C0C0',
        flexDirection:'row',
        justifyContent:'space-around',
        alignItems: 'center'
    },
    slider:{
        marginRight:10
    },
    teksCategories:{
        color:'#616D80', fontSize:16
    },
    userGreet:{
        fontSize:30
    },
    greetContainer:{
        padding:10,
        height:80,
        justifyContent:'flex-end'
    },
    teksJudulItem:{
        fontSize:12, 
        color:'#575757'
    },
    teksHargaItem:{
        fontSize:12, color:'#E76F00'
    },
    tombolPlus:{
        
        flex:3,
        justifyContent:'flex-start'
    },
    descriptionContainer:{
        flexDirection:'row',
        alignItems:'center'
    },
    plus:{
        backgroundColor:'#4B7EFF',
        padding:5,
        borderRadius:50,
        
    }
})
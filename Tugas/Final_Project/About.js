import React, {Component} from 'react';
import{
    View,
    Text,
    Image,
    StyleSheet,
    TouchableOpacity,
    TextInput,
    ScrollView
} from 'react-native';
import Icon from 'react-native-vector-icons/AntDesign';
import Icon2 from 'react-native-vector-icons/MaterialIcons';



export default () => (
            
            <View style={styles.container}>
                <View style={styles.header}>
                    <Image style={styles.img} source={require('./images/woman_small.jpg')}/>
                    <Text style={styles.teksProfile}>LAKSMANA YUDHA</Text>
                    <Text style={[styles.teksProfile, {color:'#228DF0', fontSize:14, fontWeight:'normal'}]}>React Native Developer</Text>
                </View>
                <View style={styles.body}>
                    <Text style={styles.judul}>Tentang Saya :</Text>
                    <View style={styles.boxLuar}>
                        <Text style={styles.paragraf}>
                            Halo! nama saya  Laksmana Yudha, saya tinggal di Indonesia.Sekarang saya sedang mengerjakan final project dari sanbercode
                        </Text>

                        <View style={styles.medsosContainer}>
                            <View style={styles.medsosItem}>
                                <Icon name="instagram" size={35}/>
                                <Text style={styles.teksMedsos}>@laksmanayudha_</Text>
                            </View>
                            <View style={styles.medsosItem}>
                                <Icon name="facebook-square" size={35}/>
                                <Text style={styles.teksMedsos}>Laksmana Yudha</Text>
                            </View>
                            <View style={styles.medsosItem}>
                                <Icon name="twitter" size={35}/>
                                <Text style={styles.teksMedsos}>Unknown</Text>
                            </View>
                        </View>
                    </View>
                    
                    <Text style={styles.judul}>Project :</Text>
                    <View style={styles.boxLuar}>
                        <View style={styles.medsosItem}>
                                <Icon name="gitlab" size={40}/>
                                <TouchableOpacity>
                                    <Text style={styles.teksMedsos}>https://gitlab.com/laksmanayudha/sanbercodeproject</Text>
                                </TouchableOpacity>
                        </View>
                    </View>
                    
                </View>

                <View style={styles.footer}>       
                    <View style={styles.garisBawah}/>
                    
                </View>
                
            </View>
          
)

const styles = StyleSheet.create({
    container:{
        flex: 1
    },
    header:{
        justifyContent:'center',
        alignItems:'center',
        flex:5,
    },
    body:{
        flex:1,
        paddingHorizontal:25,
        flex:7
    },
    img:{
        height:180,
        width:180,
        borderRadius:100
    },
    teksProfile:{
        fontSize:20,
        fontWeight: 'bold',
        marginTop:10
    },
    judul:{
        marginTop:10,
        fontSize:18,
        fontWeight: 'bold'
    },
    paragraf:{
        color: '#4D4D4D',
        marginTop:5,
        fontSize:14
    },
    medsosContainer:{
        paddingVertical:5
    },
    medsosItem:{
        flexDirection:'row',
        marginVertical:5,
        alignItems: 'center'
    },
    teksMedsos:{
        marginLeft:10,
        fontSize:16,
        fontWeight:'normal'
    },
    garisBawah:{
        backgroundColor:'#228DF0',
        height:5,
        width:134,
        borderRadius:2.5,
    },
    footer:{
        alignItems:'center'
    },
    boxLuar:{
        backgroundColor:'#F0F6FA',
        padding:15,
        borderRadius:6
    }

});